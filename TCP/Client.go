package TCP

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strings"
)

// Client
// 建立连接
// 数据收发
// 关闭链接
func Client() {
	conn, err := net.Dial("tcp", "127.0.0.1:20000")
	if err != nil {
		panic(fmt.Sprintf("net.Dial failed, %v\n", err))
		return
	}
	defer func(conn net.Conn) {
		err := conn.Close()
		if err != nil {
			fmt.Printf("conn.Close failed, %v\n", err)
		}
	}(conn)

	inputReader := bufio.NewReader(os.Stdin)
	for {
		//读取输入
		fmt.Printf("please input : ")
		input, _ := inputReader.ReadString('\n')
		inputInfo := strings.Trim(input, "\r\n")
		if strings.ToLower(inputInfo) == "exit" {
			fmt.Printf("exit success")
			return
		}

		_, err = conn.Write([]byte(inputInfo)) // 发送数据
		if err != nil {
			fmt.Printf("conn.Write failed, %v",err)
			break
		}

		buf := [2048]byte{}
		n, err := conn.Read(buf[:])
		if err != nil {
			fmt.Printf("conn.Read failed, %v\n", err)
			break
		}
		fmt.Printf("%v : %v\n",conn.RemoteAddr(), string(buf[:n]))
	}
}
