package TCP

import (
	"bufio"
	"fmt"
	"net"
)

// Server
// 监听端口
// 建立连接
// 协程响应
func Server() {
	//监听端口
	listen, err := net.Listen("tcp", ":20000")
	if err != nil {
		panic(fmt.Sprintf("net.Listen failed, %v\n", err))
		return
	}
	defer func(listen net.Listener) {
		err := listen.Close()
		if err != nil {
			fmt.Printf("listen.Close failed, %v\n", err)
		}
	}(listen)

	for {
		//建立连接
		conn, err := listen.Accept()
		if err != nil {
			fmt.Printf("listen.Accept failed, %v\n", err)
			continue
		}

		//协程响应
		go func(conn net.Conn) {
			defer func(conn net.Conn) {
				err := conn.Close()
				if err != nil {
					fmt.Printf("conn.Close failed, %v\n", err)
				}
			}(conn)

			for {
				reader := bufio.NewReader(conn)
				var buf [2048]byte
				n, err := reader.Read(buf[:])
				if err != nil {
					fmt.Printf("reader.Read failed, %v\n", err)
					break
				}
				recStr := string(buf[:n])
				fmt.Printf("%v : %v\n", conn.RemoteAddr(),recStr)

				backStr := fmt.Sprintf("copy %d char", len(recStr))
				_, err = conn.Write([]byte(backStr))
				if err != nil {
					fmt.Printf("conn.Write failed, %v\n", err)
					break
				}
			}

		}(conn)
	}
}