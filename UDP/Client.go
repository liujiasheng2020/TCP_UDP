package UDP

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strings"
)

func Client()  {
	conn,err := net.DialUDP("udp",nil,&net.UDPAddr{
		IP: net.IPv4(0,0,0,0),
		Port: 30000,
	})
	if err != nil {
		panic(fmt.Sprintf("net.Dial failed, %v\n", err))
		return
	}
	defer func(conn *net.UDPConn) {
		err := conn.Close()
		if err != nil {
			fmt.Printf("conn.Close failed, %v\n", err)
		}
	}(conn)

	inputReader := bufio.NewReader(os.Stdin)
	for  {
		fmt.Printf("please input : ")
		input, _ := inputReader.ReadString('\n')
		inputInfo := strings.Trim(input, "\r\n")
		if strings.ToLower(inputInfo) == "exit" {
			fmt.Printf("exit success")
			return
		}
		_,err = conn.Write([]byte(inputInfo))
		if err != nil {
			fmt.Printf("conn.Write failed, %v\n", err)
		}
		data := make([]byte,2048)
		n,Addr,err2 := conn.ReadFromUDP(data)
	if err2 != nil {
		fmt.Printf("conn.ReadFromUDP failed, %v\n", err)
		return
	}
	fmt.Printf("%v : %v\n", Addr,string(data[:n]))
	}
}