package UDP

import (
	"fmt"
	"net"
)

// Server
// 监听端口
// 建立连接
// 处理
func Server()  {
	listen,err := net.ListenUDP("udp",&net.UDPAddr{
		IP: net.IPv4(0,0,0,0),
		Port: 30000,
	})
	if err != nil {
		panic(fmt.Sprintf("net.Listen failed, %v\n", err))
		return
	}
	defer func(listen *net.UDPConn) {
		err := listen.Close()
		if err != nil {
			fmt.Printf("listen.Close failed, %v\n", err)
		}
	}(listen)

	for  {
		var data [2048]byte
		n,addr,err := listen.ReadFromUDP(data[:])
		if err != nil {
			fmt.Printf("listen.ReadFromUDP failed, %v\n", err)
			continue
		}
		fmt.Printf("%v : %v\n", addr,string(data[:n]))

		backStr := fmt.Sprintf("copy %d char", len(string(data[:n])))
		_, err = listen.WriteToUDP([]byte(backStr), addr) // 发送数据
		if err != nil {
			fmt.Printf("listen.WriteToUDP failed, %v\n", err)
			continue
		}
	}

}